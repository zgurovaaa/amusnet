package com.amusnet.demo.controller;

import com.amusnet.demo.data.*;
import com.amusnet.demo.service.*;
import jakarta.annotation.*;
import org.springframework.cache.annotation.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/amusnet")
public class AmusnetController {

	@Resource
	private PlayerService playerService;

	@Cacheable("player")
	@GetMapping("/get-player-with-largest-ggr")
	public PlayerResponse calculateGgr() {
		return playerService.getLastCreatedPlayer();
	}

}
