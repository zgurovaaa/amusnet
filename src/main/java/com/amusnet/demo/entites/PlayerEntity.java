package com.amusnet.demo.entites;

import static jakarta.persistence.TemporalType.TIMESTAMP;

import jakarta.persistence.*;
import jakarta.persistence.Id;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.*;
import org.springframework.data.annotation.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = PlayerEntity.TABLE_NAME)
public class PlayerEntity {

	public static final String TABLE_NAME = "player";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer externalId;
	private String name;
	private String surname;
	private BigDecimal ggr;
	@CreatedDate
	private Instant createdDate;

}
