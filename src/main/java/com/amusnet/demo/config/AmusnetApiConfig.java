package com.amusnet.demo.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.web.client.RestClient;

@Configuration
@Getter
public class AmusnetApiConfig {

	@Value("${amusnet.api.uri}")
	private String apiUri;

	@Bean
	public RestClient restApiClient() {
		return RestClient.create(apiUri);
	}

}
