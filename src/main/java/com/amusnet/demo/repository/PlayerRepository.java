package com.amusnet.demo.repository;

import com.amusnet.demo.entites.PlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<PlayerEntity, Integer> {

	PlayerEntity findTopByOrderByCreatedDateDesc();

}
