package com.amusnet.demo.service;

import com.amusnet.demo.data.*;
import com.amusnet.demo.entites.PlayerEntity;
import com.amusnet.demo.repository.PlayerRepository;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {

	@Resource
	private PlayerRepository playerRepository;

	protected void save(PlayerEntity playerEntity) {
		playerRepository.saveAndFlush(playerEntity);
	}

	public PlayerResponse getLastCreatedPlayer() {
		PlayerEntity playerEntity = playerRepository.findTopByOrderByCreatedDateDesc();
		return PlayerResponse.of(playerEntity);
	}
}
