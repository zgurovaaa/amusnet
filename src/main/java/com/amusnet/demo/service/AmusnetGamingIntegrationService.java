package com.amusnet.demo.service;

import com.amusnet.demo.config.AmusnetApiConfig;
import com.amusnet.demo.data.*;
import jakarta.annotation.Resource;
import java.net.*;
import java.util.*;
import java.util.stream.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.retry.annotation.*;
import org.springframework.retry.support.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.*;
import org.springframework.web.util.*;

@Service
@Transactional
public class AmusnetGamingIntegrationService {

	@Resource
	private AmusnetApiConfig apiConfig;
	@Resource
	private RestClient amusnetRestClient;
	@Resource
	private RetryTemplate retryTemplate;

	@Retryable
	public List<Player> getPlayers() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiConfig.getApiUri() + "/players")
			.queryParam("page", 0)
			.queryParam("pageSize", 20);
		URI uri = builder.build().encode().toUri();
		return retryTemplate.execute(context -> amusnetRestClient.get()
			.uri(uri)
			.accept(MediaType.APPLICATION_JSON)
			.retrieve()
			.toEntity(new ParameterizedTypeReference<List<Player>>() {
			})).getBody();

	}

	public List<GameActivity> getGameActivities(List<Integer> playerIds) {
		ResponseEntity<List<GameActivity>> responseEntity = requestGameActivities(playerIds, 0);
		List<GameActivity> gameActivities = new ArrayList<>();
		if (responseEntity.getStatusCode().equals(HttpStatus.OK) && responseEntity.getBody() != null) {
			gameActivities.addAll(responseEntity.getBody());
		}

		int page = 1;
		while (responseEntity.hasBody() && !responseEntity.getBody().isEmpty()) {
			responseEntity = requestGameActivities(playerIds, page);
			page++;
			if (responseEntity.getStatusCode().equals(HttpStatus.OK) && responseEntity.getBody() != null) {
				gameActivities.addAll(responseEntity.getBody());
			}
		}
		return gameActivities;
	}

	@Retryable
	private ResponseEntity<List<GameActivity>> requestGameActivities(List<Integer> playerIds, int page) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiConfig.getApiUri() + "/game-activity")
			.queryParam("page", page)
			.queryParam("pageSize", 100)
			.queryParam("playerIds", playerIds.stream().map(String::valueOf)
				.collect(Collectors.joining(",")));
		URI uri = builder.build().encode().toUri();
		return retryTemplate.execute(context -> amusnetRestClient.get()
				.uri(uri)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.toEntity(new ParameterizedTypeReference<List<GameActivity>>() {
				}));
	}

}
