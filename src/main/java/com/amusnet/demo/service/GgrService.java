package com.amusnet.demo.service;

import com.amusnet.demo.data.*;
import com.amusnet.demo.entites.PlayerEntity;
import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.context.event.*;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.*;

@Service
@Transactional
public class GgrService {

	@Resource
	private AmusnetGamingIntegrationService amusnetGamingIntegrationService;
	@Resource
	private PlayerService playerService;

	@Value("${schedule.enabled.get-gaming-data}")
	private boolean getGamingDataEnabled;

	@EventListener(ApplicationReadyEvent.class)
	@Scheduled(cron = "${schedule.cron.get-gaming-data}")
	public void onSchedule() {
		if (getGamingDataEnabled) {
			calculateGamingData();
		}
	}

	public void calculateGamingData() {
		List<Player> players = amusnetGamingIntegrationService.getPlayers();
		List<Integer> playerIds = players.stream().map(Player::id).toList();
		List<GameActivity> gameActivities = amusnetGamingIntegrationService.getGameActivities(playerIds);
		calculateGgr(players, gameActivities);

	}

	private void calculateGgr(List<Player> players, List<GameActivity> gameActivities) {
		Map<Integer, List<GameActivity>> gameActivitiesByPlayer = new HashMap<>();
		gameActivities.forEach(gameActivity -> {
			if (gameActivitiesByPlayer.containsKey(gameActivity.id())) {
				gameActivitiesByPlayer.get(gameActivity.playerId()).add(gameActivity);
			} else {
				gameActivitiesByPlayer.put(gameActivity.playerId(), new ArrayList<>(List.of(gameActivity)));
			}
		});
		Map<Integer, BigDecimal> ggrs = new HashMap<>();
		gameActivitiesByPlayer.forEach((player, activities) -> {
			BigDecimal totalWins = activities.stream().map(GameActivity::winAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
			BigDecimal totalBets = activities.stream().map(GameActivity::betAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
			ggrs.put(player, totalWins.subtract(totalBets));
			});

		Map.Entry<Integer, BigDecimal> maxGgrEntry = ggrs.entrySet().stream().max(Map.Entry.comparingByValue()).get();

		Optional<Player> opt = players.stream().filter(p -> p.id().equals(maxGgrEntry.getKey())).findFirst();
		if (opt.isEmpty()) {
			return;
		}
		PlayerEntity playerEntity = Player.of(opt.get());
		playerEntity.setGgr(maxGgrEntry.getValue());
		playerService.save(playerEntity);
	}

}
