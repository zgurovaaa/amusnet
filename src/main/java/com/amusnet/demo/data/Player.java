package com.amusnet.demo.data;

import com.amusnet.demo.entites.PlayerEntity;
import java.math.*;

public record Player(Integer id, String name, String surname) {

	public static PlayerEntity of(Player player) {
		PlayerEntity res = new PlayerEntity();
		res.setExternalId(player.id);
		res.setName(player.name);
		res.setSurname(player.surname);
		return res;
	}

}
