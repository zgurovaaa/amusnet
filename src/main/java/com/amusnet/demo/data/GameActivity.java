package com.amusnet.demo.data;

import java.math.BigDecimal;

public record GameActivity(Integer id, Integer playerId, BigDecimal betAmount, BigDecimal winAmount, String currency) {

}
