package com.amusnet.demo.data;

import com.amusnet.demo.entites.*;
import java.math.*;

public record PlayerResponse(Integer id, String name, String surname, BigDecimal ggr) {
	public static PlayerResponse of(PlayerEntity entity) {
		return new PlayerResponse(entity.getExternalId(), entity.getName(), entity.getSurname(), entity.getGgr());
	}

}
