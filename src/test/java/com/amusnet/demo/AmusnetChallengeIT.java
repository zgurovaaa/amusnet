package com.amusnet.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.amusnet.demo.data.*;
import java.net.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.context.SpringBootTest.*;
import org.springframework.boot.test.web.client.*;
import org.springframework.boot.test.web.server.*;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.*;
import org.springframework.web.util.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class AmusnetChallengeIT {

	@LocalServerPort
	int port;
	private static final TestRestTemplate template = new TestRestTemplate();

	@Test
	public void testEndpoint() throws InterruptedException {
		Thread.sleep(2000);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/amusnet/get-player-with-largest-ggr");
		URI uri = builder.build().encode().toUri();
		ResponseEntity<PlayerResponse> response = template.exchange(uri, HttpMethod.GET, null, PlayerResponse.class);

		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNotNull(response.getBody());
		assertNotNull(response.getBody().ggr());
	}

}
