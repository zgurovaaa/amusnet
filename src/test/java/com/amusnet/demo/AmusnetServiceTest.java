package com.amusnet.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.amusnet.demo.data.*;
import com.amusnet.demo.service.*;
import jakarta.annotation.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.springframework.boot.test.context.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class AmusnetServiceTest {

	@Mock
	private AmusnetGamingIntegrationService amusnetGamingIntegrationService;

	@Resource
	private GgrService ggrService;
	@Resource
	private PlayerService playerService;

	@Test
	public void testCalculateGgr() {
		ggrService.calculateGamingData();
		PlayerResponse playerResponse = playerService.getLastCreatedPlayer();
		assertNotNull(playerResponse);
	}



}
