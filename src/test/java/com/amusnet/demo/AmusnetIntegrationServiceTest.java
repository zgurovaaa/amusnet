package com.amusnet.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.amusnet.demo.data.*;
import com.amusnet.demo.service.*;
import jakarta.annotation.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.junit.jupiter.*;
import org.springframework.test.context.transaction.*;
import org.springframework.transaction.annotation.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class AmusnetIntegrationServiceTest {

	@Resource
	private AmusnetGamingIntegrationService amusnetGamingIntegrationService;

	@Test
	public void testGetPlayers() {
		List<Player> players = amusnetGamingIntegrationService.getPlayers();
		TestTransaction.flagForCommit();
		TestTransaction.end();
		assertNotNull(players);
	}

	@Test
	public void testGetGameActivities() {
		List<Player> players = amusnetGamingIntegrationService.getPlayers();
		List<Integer> playerIds = players.stream().map(Player::id).toList();
		List<GameActivity> gameActivities = amusnetGamingIntegrationService.getGameActivities(playerIds);
		TestTransaction.flagForCommit();
		TestTransaction.end();
		assertNotNull(gameActivities);
	}


}
