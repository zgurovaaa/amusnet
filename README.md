* To create the default MySQL database and user
    + create database `amusnet` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
    + CREATE USER 'admin'@'localhost' IDENTIFIED BY 'secret@';
    + FLUSH PRIVILEGES;
    + GRANT ALL ON `amusnet`.* TO 'admin'@'localhost';
    + FLUSH PRIVILEGES;
